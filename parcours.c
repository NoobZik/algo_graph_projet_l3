/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parcours.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/25 23:40:56 by noobzik           #+#    #+#             */
/*   Updated: 2019/12/22 20:26:53 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "graphe-4.h"
#include "parcours.h"
#include "conteneur_sommets.h"


/**
 * Libère l'espace alloué par le msuc_file
 * @param m Un pointeur vers un msuc file
 */
void free_msuc_file(msuc_file *m) {
  free(m->tab);
  free(m);
}

/**
 * Met en mémoire un successeur du sommet a la position du sommet
 * @param a      Un pointeur vers un msuc_file
 * @param m      Un pointeur vers le succeseur m du sommet
 * @param sommet Le sommet de type int qui est aussi l'indice du tableau
 */
void stack_msuc_file(msuc_file *a, msuc *m, int sommet) {
  a->tab[sommet] = m;
}

/**
 * Retourne le msuc mis en mémoire de l'indice donnée par le sommet
 * @param  a      Un pointeur vers le msuc_file
 * @param  sommet Le sommet de type int qui est aussi l'indice du tableau
 * @return        Un msuc qui se trouve a l'indice sommet du tableau des msuc_file
 */
msuc *last_msuc_file(msuc_file *a, int sommet) {
  msuc *m = a->tab[sommet];
  return m;
}

/**
 * Initialise un tableau à n sommet pour garder en mémoire les successeurs d'un
 * sommet lors du parcours générique.
 * @return  Un msuc s'il existe, ou NULL
 */
msuc_file *init_msuc_file(void) {
  msuc_file *m = malloc(sizeof(msuc_file));
  if (!m)
    return NULL;
  return m;
}

/******************************************************************************/

struct parcours *pc_init(graphe *g, conteneur_sommets *cs, int *prio) {
  struct parcours* p;
  assert(g);
  assert(cs);
  if (!(p = malloc(sizeof(struct parcours)))) {
    perror("Erreur malloc pc_init");
    return NULL;
  }
  p->sharir_inv = false;
  p->acyclique = true;
  p->sharir = false;

  assert(p);

  p->mf              = init_msuc_file();
  if (!p->mf) {
    perror("init_msuc_file malloc :");
    free(p);
    return NULL;
  }

  assert(p->mf);

  p->mf->tab = calloc((size_t) graphe_get_n(g) * 2, sizeof(msuc *));
  if (!(p->mf->tab)) {
    perror("p->mf->tab");
    free(p->mf);
    free(p);
    return NULL;
  }

  assert(p->mf->tab);

  if (prio == NULL) {
    p->prio = malloc(sizeof(int) * (size_t) graphe_get_n(g));
    if (!p->prio) {
      perror("p->prio malloc ;");
      free_msuc_file(p->mf);
      free(p);
      return NULL;
    }
    for (int i = 0; i < graphe_get_n(g); i++) {
      p->prio[i]     = i;
    }
  }
  else {
    p->prio = prio;
  }

  assert(p->prio);

  p->g               = g;
  p->conteneur       = cs;
  p->number_explored = 0;
  p->mark_protect    = 0;
  p->mark            = calloc((size_t) graphe_get_n(g), sizeof(int));

  if (!(p->mark)) {
    perror("p->mark malloc :");
    free_msuc_file(p->mf);
    free(p->prio);
    free(p);
    return NULL;
  }

  assert(p->mark);

  p->explored        = calloc((size_t) graphe_get_n(g), sizeof(int));
  if (!(p->explored)) {
    free_msuc_file(p->mf);
    free(p->mark);
    free(p->prio);
    free(p);
    return NULL;
  }
  assert(p->explored);

  p->pref            = pile_creer(graphe_get_n(g));
  if (!p->pref) {
    perror("p->pref init");
    free_msuc_file(p->mf);
    free(p->explored);
    free(p->mark);
    free(p->prio);
    free(p);
    return NULL;
  }

  assert(p->pref);

  p->suff            = file_creer(graphe_get_n(g));
  if (!p->suff) {
    perror("p->suff");
    pile_detruire(p->pref);
    free_msuc_file(p->mf);
    free(p->explored);
    free(p->mark);
    free(p->prio);
    free(p);
    return NULL;
  }

  assert(p->suff);

  p->N               = file_creer(graphe_get_n(g));
  if (!p->N) {
    perror("p->N");
    file_detruire(p->suff);
    pile_detruire(p->pref);
    free_msuc_file(p->mf);
    free(p->explored);
    free(p->mark);
    free(p->prio);
    free(p);
    return NULL;
  }

  p->kosa = pile_creer(graphe_get_n(g));
  if (!p->kosa) {
    perror("p->kosa");
    file_detruire(p->N);
    file_detruire(p->suff);
    pile_detruire(p->pref);
    free_msuc_file(p->mf);
    free(p->explored);
    free(p->mark);
    free(p->prio);
    free(p);
    return NULL;
  }

  assert(p->N);

  p->racine_R = calloc((size_t) graphe_get_n(g), sizeof(int));
  assert(p->racine_R);
  p->marked_since = malloc((size_t) graphe_get_n(g) * sizeof(int));
  assert(p->marked_since);
  for (int i = 0; i < graphe_get_n(g); i++) {
    p->marked_since[i] = -1;
  }
  return p;
}

/**
 * Détruit proprement un parcours conteneur
 * @param p Une structure de parcours à détruire
 */
void pc_detruire(struct parcours *p) {
  if (!p) return;
  pile_detruire(p->kosa);
  free_msuc_file(p->mf);
  free(p->prio);
  pile_detruire(p->pref);
  file_detruire(p->suff);
  file_detruire(p->N);
  free(p->mark);
  free(p->marked_since);
  free(p->explored);
  free(p->racine_R);
  free(p);
}

/**
 * [pc_choisir_racine description]
 * @param  p Une structure de parcours
 * @return La valeur de retour de cs_choisir
 */
int pc_choisir_racine(struct parcours *p) {
  int r = cs_choisir(p->conteneur);
  return r;
}

/**
 * [pc_est_fini description]
 * @param  p Une structure de parcours
 * @return   Renvoie 1 si le parcours est fini
 *           Renvoie 0 si non.
 */
int pc_est_fini(struct parcours *p){
  return ((graphe_get_n(p->g)) == (p->number_explored)) ? 1 : 0;
}

/**
 * Fonction qui marque un sommet en mémoire dans un tableau des sommets marqués
 * @param p      Une structure de parcours
 * @param sommet Un sommet à marquer
 */
void pc_marquer_comme_visite(struct parcours *p, int sommet) {
  if (p->mark[sommet] == 0) {
    p->mark[sommet] = 1;
    p->mark_protect += 1;
  }
  else {
    // puts("C'est déjà marqué WTF go away bitch");
  }
}

void pc_marquer_comme_visite_depuis(struct parcours *p, int sommet, int depuis) {
  if (p->marked_since[sommet] == -1)
    p->marked_since[sommet] = depuis;
}

void pc_marquer_comme_explore(struct parcours *p, int sommet) {
   if (p->explored[sommet] == 0) {
    p->explored[sommet] = 1;
    p->number_explored += 1;
  }
  else {
    // puts("OULA CEST DEJA EXPLORER BIATCH");
  }
}

/**
 * Le nom de la fonction s'explique d'elle même
 * @param  p Une structure de parcours
 * @return 1 Si le conteneur est vide
 *         0 Si le conteneur n'est pas vide
 */
int pc_conteneur_est_vide(struct parcours *p) {
  return (cs_est_vide(p->conteneur)) ? 1 : 0;
}

/**
 * Ajoute un sommet dans un conteneur
 * @param p      Une structure de parcours
 * @param sommet Un sommet à ajouter dans le parcours
 */
void pc_ajouter_dans_conteneur(struct parcours *p, int sommet) {
  cs_ajouter(p->conteneur, sommet);
}

/**
 * Supprime un sommet du conteneur
 * @param p Une structure de parcours
 */
void pc_supprimer_du_conteneur(struct parcours *p) {
  cs_supprimer(p->conteneur);
}

/**
 * [pc_choisir_dans_conteneur description]
 * @param  p Une structure de parcours
 * @return   Renvoie int positive ou nul pour un sommet existant dans le conteneur
 *           Renvoie -1 si le conteneur est vide
 */
int pc_choisir_dans_conteneur(struct parcours *p) {
  return pc_conteneur_est_vide(p) == 0 ? cs_choisir(p->conteneur) : -1;
}

/**
 * Cette fonction retourne le prchain successeur du sommet
 * @param  p      Une structure de parcours
 * @param  sommet Un sommet dont on veut connaitre un successeur
 * @return        Un pointeur vers le maillon succeseur existant.
 *                Null s'il en existe pas.
 */
msuc *pc_prochain_msuc(struct parcours *p, int sommet) {
  msuc *m;
  // printf("Je rentre pour la première fois ici, ma valeur est %d\n", sommet);
  if ((m = msuc_suivant(last_msuc_file(p->mf, sommet))) == NULL) {
    m = graphe_get_prem_msuc(p->g, sommet);
    // printf("Je ne me suis pas trouve un successeur du tablea, je prendre le premier %d\n", (m) ? msuc_sommet(m) : -1 );
    stack_msuc_file(p->mf, m, sommet);
  }
  while (m != NULL) {
    if (msuc_sommet(m) == sommet) {
      // printf("Bordel, le successeur %d est %d", sommet, msuc_sommet(m));
      p->acyclique = false;
      m = msuc_suivant(m);
      // printf("Du coup en cherchant un nouveau, le %d est %d\n", sommet,  (m) ? msuc_sommet(m) : -1);
      stack_msuc_file(p->mf, m, sommet);
    }
    else if (pc_est_visite(p, msuc_sommet(m))) {
      // printf("Oh shit, le succeseur de %d est déjà visité %d\n", sommet, msuc_sommet(m));
      for (msuc *l = graphe_get_prem_msuc(p->g, msuc_sommet(m)); l != NULL; l = msuc_suivant(l)) {
        if (msuc_sommet(l) == sommet) {
          p->acyclique = false;
          break;
        }
      }
      m = msuc_suivant(m);
      // printf("Je passe donc au suivant qui est %d\n", (m) ? msuc_sommet(m) : -1);
      stack_msuc_file(p->mf, m, sommet);
    }
    else break;
  }
  return m;
}

/**
 * Fonction qui vérifie que le sommet passé en argument est marqué ou pas durant
 * le parcours.
 * @param  p      Une structure de parcours
 * @param  sommet Le sommet dont le marquage est à vérifier
 * @return        1 si oui
 *                0 si non
 */
int pc_est_visite(struct parcours *p, int sommet) {
  return p->mark[sommet];
}

/**
 * Cette partie à besoin d'être nettoyé !
 * Ne respecte pas encore l'algo demandé du sujet
 */
void pc_parcourir_depuis_sommet(struct parcours *p, int r) {
  int v, w;
  msuc *m;

  pile_empiler(p->pref, r);
  p->racine_R[r] = 1;
   // printf("Je vais mettre r = %d dans la pile\n", r);
  if (p->explored[r] == 1) {
    // puts("C'est exploré biatch go away");
    return;
  }
  pile_empiler(p->kosa, r);
  file_enfiler(p->suff, r);
  pc_marquer_comme_visite(p,r);
  if (p->pc_distance)
    p->dist[r] = 0;

  while ((pc_conteneur_est_vide(p) == 0)) {
    v = pc_choisir_dans_conteneur(p);
    // p->racine_R[v] = 1;
    // printf("Allons chercher un successeur de v = %d\n", v);
    // printf("%d Est t-il marqué ? %d\n", v, p->mark[v]);
    // printf("%d Est t-il exploré ? %d\n", v, p->explored[v]);
    if (p->sharir_inv)
      if (p->explored[v]) {
        // puts("WHY ARE YOU HERE, GTFO YOU EXPLORED SHIT");
        pc_supprimer_du_conteneur(p);
        return;
      }


    // if (p->marked_since[v] == v) {
      // printf("CYCLE !! Je vais depiler dans la file = %d dans la pile\n", pile_sommet(p->kosa));
      // pc_marquer_comme_explore(p, v);
      //
      // pc_supprimer_du_conteneur(p);
      // file_enfiler(p->N, pile_depiler(p->kosa));
      // continue;
    // }

    m = pc_prochain_msuc(p, v);
    if (m) {
      if (!pc_est_visite(p, w = msuc_sommet(m))) {
        pc_marquer_comme_visite(p,w);
        pc_ajouter_dans_conteneur(p,w);
        if (p->sharir_inv)
          graphe_ajouter_arc(*p->arbo, w, v, 1.0);
        else
          graphe_ajouter_arc(*p->arbo, v, w, 1.0);
        // printf("Je vais mettre w = %d dans la pile\n", w);
        pc_marquer_comme_visite_depuis(p, w, r);
        pile_empiler(p->pref, w);
        pile_empiler(p->kosa, w);
        file_enfiler(p->suff, w);
        if (p->pc_distance)
           p->dist[w] = p->dist[v] + 1;
      }
    }
    else {
      // printf("\033[0;31m");
      // printf("Na pas de successeurs %d\n", v);
      // printf("\033[0m");
      pc_marquer_comme_explore(p, v);
      pc_supprimer_du_conteneur(p);
      if (!pile_est_vide(p->kosa)) {
        // printf("Je vais depiler dans la file = %d dans la pile\n", pile_sommet(p->kosa));
        file_enfiler(p->N, pile_depiler(p->kosa));
      }
      else {
        // printf("\033[1;36m");
        // puts("GENIUS, DEPILER UN TRUC VIDE !!");
        // printf("\033[0m");
        if (p->mark[v] == 0) {
          // printf("\033[01;33m");
          // puts("Bon c'était pas marqué");
          // printf("\033[0m");
          pc_marquer_comme_visite(p, v);
          file_enfiler(p->N, v);
        }
      }
    }
    // printf("\033[0;32m");
    // puts("Iteration fini");
    // printf("\033[0m");
  }
  // printf("\033[01;35m");
  // puts("SI JARRIVE ICI CEST QUE LE CONTENEUR EST VIDE WTF ");
  // printf("\033[0m");
}

void pc_parcourir(struct parcours *p) {
  int r = 0;
  pc_ajouter_dans_conteneur(p, r);
//  pc_marquer_comme_visite(p, r);

  while (!(pc_est_fini(p)) && r!=-1) {
    pc_parcourir_depuis_sommet(p, r);
    // if (p->acyclique == false) {
      // printf("PROTECTOR_protect  : %d\n", p->mark_protect);
      // printf("PROTECTOR_explored  : %d\n", p->number_explored);
      // printf("PROTECTOR_graphe_get_n : %d\n", graphe_get_n(p->g));
//
      // return;
    // }
    // puts("Liste des explorés");
    for (int i = 0; i < graphe_get_n(p->g); i++) {
       // printf("%d  ", p->explored[i]);
      if (p->explored[i] == 0) {
        pc_ajouter_dans_conteneur(p, i);
      }
     }
     // puts("Liste des marqués");
     // for (int i = 0; i < graphe_get_n(p->g); i++) {
       // printf("%d  ", p->mark[i]);
     // }
     // puts("");
    if (!(pc_conteneur_est_vide(p))) {
      // printf("%d\n\n", r);
      r = pc_choisir_dans_conteneur(p);
      p->racine_R[r] = 1;
    }
  }
  // printf("PROTECTOR_protect  : %d\n", p->mark_protect);
  // printf("PROTECTOR_explored  : %d\n", p->number_explored);
  // printf("PROTECTOR_graphe_get_n : %d\n", graphe_get_n(p->g));
//

}
