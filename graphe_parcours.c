#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pile.h"
#include "graphe_parcours.h"
#include "graphe-4.h"
#include "parcours.h"
#include "conteneur_sommets.h"
#include <assert.h>
#include <stdbool.h>

/* fonctionnel / non fini a cause de suff */

int graphe_parcours_profondeur(graphe *g, graphe **arbo, int **suff, int *prio) {
  conteneur_sommets *cs = cs_creer_pile(graphe_get_n(g));

  if (!cs)
    return -1;
  assert(cs);

  struct parcours   *p = pc_init(g, cs, prio);
  if (!p) {
    cs_detruire(cs);
    return -1;
  }

  assert(p);

  p->pc_distance = false;
  p->sharir = false;
  p->sharir_inv = false;

  //dépiler suff dans int* tab alloué ici, dans le bon ordre et, (*suff) = tab

  *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = arbo;
  pc_parcourir(p);

  // A regler, mauvais suff
  int t = file_taille(p->suff);
  *suff = file_tabint(p->suff, &t);


  cs_detruire(cs);
  pc_detruire(p);
  return 0;
}

int graphe_parcours_largeur(graphe *g, graphe **arbo, int **suff, int *prio) {
  conteneur_sommets *cs = cs_creer_file(graphe_get_n(g));

  if (!cs)
    return -1;
  assert(cs);

  struct parcours   *p = pc_init(g, cs, prio);
  if (!p) {
    cs_detruire(cs);
    return -1;
  }

  assert(p);

  p->pc_distance = false;
  p->sharir = false;
  p->sharir_inv = false;

  //dépiler suff dans int* tab alloué ici, dans le bon ordre et, (*suff) = tab

  *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = arbo;
  pc_parcourir(p);

  // A regler, mauvais suff
  int t = file_taille(p->suff);
  *suff = file_tabint(p->suff, &t);


  cs_detruire(cs);
  pc_detruire(p);
  return 0;
}

int graphe_parcours_larg_ou_prof(graphe *g, graphe **arbo, int **suff, int *prio) {
  conteneur_sommets *cs = cs_creer_pile_ou_file(graphe_get_n(g));

  if (!cs)
    return -1;
  assert(cs);

  struct parcours   *p = pc_init(g, cs, prio);
  if (!p) {
    cs_detruire(cs);
    return -1;
  }

  assert(p);

  p->pc_distance = false;
  p->sharir = false;
  p->sharir_inv = false;
  p->pc_distance = false;

  //dépiler suff dans int* tab alloué ici, dans le bon ordre et, (*suff) = tab

  *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = arbo;
  pc_parcourir(p);

  // A regler, mauvais suff
  int t = file_taille(p->suff);
  *suff = file_tabint(p->suff, &t);


  cs_detruire(cs);
  pc_detruire(p);
  return 0;
}

int graphe_ordre_top(graphe *g, int** ordre_top){
  conteneur_sommets *cs = cs_creer_pile(graphe_get_n(g));

  if (!cs)
    return -1;
  assert(cs);
  struct parcours   *p = pc_init(g, cs, NULL);
  if (!p) {
    cs_detruire(cs);
    return -1;
  }

  assert(p);

  p->pc_distance = false;
  p->sharir = false;
  p->sharir_inv = false;
  //dépiler suff dans int* tab alloué ici, dans le bon ordre et, (*suff) = tab

  graphe *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = &arbo;
  pc_parcourir(p);


  if (p->acyclique == true) {
    *ordre_top = malloc(sizeof(int) * (size_t) graphe_get_n(g));

    for (int i = 0; i < graphe_get_n(g); i=i+2) {
      if (pile_taille(p->pref)) {
        (*ordre_top)[i] = pile_depiler(p->pref);
      }
      else {
        break;
      }
      if (i+1 < graphe_get_n(g) && file_taille(p->suff)) {
        (*ordre_top)[i+1] = file_defiler(p->suff);
      }
      else break;
    }
  }
  else {
    *ordre_top = NULL;
  }


  graphe_liberer(arbo);
  cs_detruire(cs);
  pc_detruire(p);
  return 0;
}

/*int *graphe_creer_comp_conn(graphe *g)
{
	int u, v;
	mat_bool *m = mat_bool_creer_clot_trans(g);
	int *comp_conn = malloc(sizeof(int) * graphe_get_n(g));

	if (m == NULL || comp_conn == NULL)
		return NULL;

	for (u = 0; u < graphe_get_n(g); ++u)
		comp_conn[u] = -1; // les sommets sont non marqués

	for (u = 0; u < graphe_get_n(g); ++u)
		if (comp_conn[u] == -1)
			for (v = 0; v < graphe_get_n(g); ++v)
				if (mat_bool_get_coeff(m, u, v))
					comp_conn[v] = u;

	mat_bool_detruire(m);
	return comp_conn;
}*/



int graphe_comp_fort_conn(graphe *g, int **reprs_cfc) {
  conteneur_sommets *cs = cs_creer_pile(graphe_get_n(g));

  if (!cs)
    return -2;
  assert(cs);

  struct parcours   *p = pc_init(g, cs, NULL);
  if (!p) {
    cs_detruire(cs);
    return -2;
  }

  assert(p);
  //dépiler suff dans int* tab alloué ici, dans le bon ordre et, (*suff) = tab
  p->sharir = true;
  p->sharir_inv = false;
  p->pc_distance = false;
  graphe *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = &arbo;
  pc_parcourir(p);

   int t = file_taille(p->N);
   // printf("%d\n", t);
   int *tab = file_tabint(p->N, &t);
//  int *tab = malloc(sizeof(int) * (size_t) graphe_get_n(g));


  // puts("");
  // graphe_ecrire_dot(arbo, "parcours_sharir_depart.dot", 0);
  // system("dot -T pdf parcours_sharir_depart.dot -O");
  graphe *arbo_2 = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  graphe *tmp = *p->arbo;
  *p->arbo = arbo_2;
  arbo_2 = tmp;
  printf("\n\n\n\n\n\n");

  pc_detruire(p);

  p = pc_init(arbo_2, cs, NULL);
  if (!p) {
    cs_detruire(cs);
    return -2;
  }

  p->arbo = &arbo;
  p->sharir_inv = true;
  p->sharir = false;
  pc_parcourir(p);

  // graphe_ecrire_dot(arbo, "parcours_sharir_apres.dot", 0);
  // system("dot -T pdf parcours_sharir_apres.dot -O");

  *reprs_cfc = tab;

  puts("REPRESENTANT R");
  for (int i = 0; i < graphe_get_n(g); i++) {
    printf("%d -- ", p->racine_R[i]);
  }
  puts("");

//  free(tab);
  cs_detruire(cs);
  graphe_liberer(arbo_2);
  graphe_liberer(arbo);
  pc_detruire(p);
  return 0;
}

int *graphe_distances_depuis_sommet(graphe *g, int r)
{
  conteneur_sommets *cs = cs_creer_pile(graphe_get_n(g));
  struct parcours *p = pc_init(g, cs, NULL);
  graphe *arbo = graphe_creer(graphe_get_n(g), graphe_est_or(g));
  p->arbo = &arbo;
  int *dist = calloc((size_t) graphe_get_n(g), sizeof(int));
  p->dist = dist;
  p->pc_distance = true;
  assert(p->dist);

  //  p->sommet = r;
  pc_ajouter_dans_conteneur(p, r);
  pc_marquer_comme_visite(p, r);
  pc_parcourir_depuis_sommet(p,r);

  for (int j = 0; j < g->n; j++) {
    if (p->dist[j] == 0 && j!=r) {
      p->dist[j] = DIST_INF;
    }
  }
  graphe_liberer(arbo);
  pc_detruire(p);
  cs_detruire(cs);
  return dist;
}



int graphe_diametre(graphe *g)
{
  int i,j;
  int max = 0;
  int **dist = malloc((size_t) graphe_get_n(g) * sizeof(int));

  for ( i = 0; i < g->n; i++) {
    dist[i] = graphe_distances_depuis_sommet(g,i);
    for ( j = 0; j < g->n; j++) {
      if (dist[i][j] > max) {
        max = dist[i][j];
      }
      /*if (dist[j] == -1) {
        return DIST_INF;
      }*/
      //a voir avec composantes connexes
      // voir si diametre se fait apres le parcours ou avant
    }
  }
  for (i = 0; i < graphe_get_n(g); i++) {
    int *currentIntPtr = dist[i];
    free(currentIntPtr);
  }
  free(dist);
  return max;
}
